// Setup dependencies
const express = require('express')
const mongoose = require('mongoose')
const cors = require('cors')
require('dotenv').config() // Initialize ENV

// MongoDB Connection
mongoose.connect(`mongodb+srv://admin:${process.env.MONGODB_PASSWORD}@wdc028-course-booking.jvcc8cx.mongodb.net/Capstone-2?retryWrites=true&w=majority`, {
	useNewUrlParser: true,
	useUnifiedTopology: true
})

mongoose.connection.once('open', () => console.log("Connected to MongoDB!"))

// Server Setup
const app = express()

// Middleware
app.use(cors())
app.use(express.json())
app.use(express.urlencoded({extended: true}))

// Server Listening
app.listen(process.env.PORT || 3000, () => {
	console.log(`API is now running on port ${process.env.PORT || 3000}`)
})